### LoRaWAN Indoor Light Energy Harvesting - Air Quality Node
Harvesting energy plays an important role in increasing the efficiency and lifetime of IoT devices. Energy harvesting systems have some limitations such as unavailability of the energy source from which energy is supposed to be harvested, low amount of harvested energy, inefficiency of the harvesting system, etc. To overcome these limitations, some efforts have been done and new models for harvesting energy have been formed which are discussed in this review concerning the energy source of the harvest. Indoor Photo-voltaic Cell is used to harvest energy from indoor lighting. The harvested energy is stored into a Li-ion battery which is able to supply required amount of power to LoRaWAN Air Quality node based on BME680.

### Prerequisites
1. LoRaWAN & STM32 based Controller: https://gitlab.com/icfoss/OpenIoT/c1_dev_v1.0
2. Photo-Voltaic Cell - AM1815 (5.0V): https://industry.panasonic.eu/products/energy-building/amorphous-solar-cells/amorton-indoor-environment
3. Energy Harvester Generic Board for LPWAN: https://gitlab.com/soorajvs13/energy-harvester-generic-board-for-lpwan
4. 14mAh Battery: https://www.mouser.in/ProductDetail/Nichicon/SLB08115L1401PM?qs=DRkmTr78QARoHNFnJd5OYQ%3D%3D
5. BME680: https://www.bosch-sensortec.com/products/environmental-sensors/gas-sensors/bme680/
6. Source Code: https://gitlab.com/soorajvs13/bme680-stm32-integration

